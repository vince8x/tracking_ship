    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo (isset($vehicledetails)) ? 'Sửa' : 'Thêm phương tiện' ?>
            </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url(); ?>/dashboard">Phương tiện</a></li>
              <li class="breadcrumb-item active"><?php echo (isset($vehicledetails)) ? 'Sửa phương tiện' : 'Thêm phương tiện' ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form method="post" id="vehicle_add" class="card" action="<?php echo base_url(); ?>vehicle/<?php echo (isset($vehicledetails)) ? 'updatevehicle' : 'insertvehicle'; ?>">
          <div class="card-body">
            <div class="row">
              <input type="hidden" name="v_id" id="v_id" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_id'] : '' ?>">

              <div class="col-sm-6 col-md-4">
                <label class="form-label">Số đăng kiểm</label>
                <div class="form-group">
                  <input type="text" name="v_registration_no" id="v_registration_no" class="form-control" placeholder="Số đăng kiểm" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_registration_no'] : '' ?>">
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <label class="form-label">Tên phương tiện</label>
                <div class="form-group">
                  <input type="text" name="v_name" id="v_name" class="form-control" placeholder="Tên" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_name'] : '' ?>">
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label class="form-label">Chủ phương tiện</label>
                  <input type="text" name="v_manufactured_by" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_manufactured_by'] : '' ?>" class="form-control" placeholder="Chủ phương tiện">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label class="form-label">Loại phương tiện</label>
                  <select id="v_type" name="v_type" class="form-control " required="">
                    <option value="">Chọn loại</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_type'] == 'PASSENGER_SHIP_SEA') ? 'selected' : '' ?> value="PASSENGER_SHIP_SEA">Tàu biển - Chở Khách</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_type'] == 'CARGO_SHIP') ? 'selected' : '' ?> value="CARGO_SHIP">Tàu Thuỷ - Chở Hàng</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_type'] == 'PASSENGER_SHIP_RIVER') ? 'selected' : '' ?> value="PASSENGER_SHIP_RIVER">Tàu Thủy - Chở Khách</option>
                  </select>

                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label for="v_color" class="form-label">Màu<small> (Để hiện trên bản đồ)</small></label>
                  <input id="add-device-color" name="v_color" class="jscolor {valueElement:'add-device-color', styleElement:'add-device-color', hash:true, mode:'HSV'} form-control" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_color'] : '#F399EB' ?>" required>
                </div>
              </div>

              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label for="v_is_active" class="form-label">Trạng thái</label>
                  <select id="v_is_active" name="v_is_active" class="form-control " required="">
                    <option value="">Chọn trạng thái</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_is_active'] == 1) ? 'selected' : '' ?> value="1">Hoạt động</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_is_active'] == 0) ? 'selected' : '' ?> value="0">Không hoạt động</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-6 col-md-4 d-none">
                <div class="form-group">
                  <label class="form-label">Registration Expiry Date</label>
                  <input type="text" required="" name="v_reg_exp_date" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_reg_exp_date'] : '' ?>" class="form-control datepicker" placeholder="Registration Expiry Date">
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label for="v_group" class="form-label">Số ghế/ Tấn đăng kiểm</label>
                  <select id="v_group" name="v_group" class="form-control " required="">
                    <option value="">Chọn số đăng kiểm</option>
                    <?php if (!empty($v_group)) {
                      foreach ($v_group as $v_groupdata) { ?>
                        <option <?= (isset($vehicledetails[0]['v_group']) && $vehicledetails[0]['v_group'] == $v_groupdata['gr_id']) ? 'selected' : '' ?> value="<?= $v_groupdata['gr_id'] ?>"><?= $v_groupdata['gr_name'] ?></option>
                    <?php }
                    } ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <label class="form-label">Tuyến hoạt động</label>
                <div class="form-group">
                  <select id="v_route" name="v_route" class="form-control " required="">
                    <option value="">Chọn tuyến</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_route'] == 'ROUTE_1') ? 'selected' : '' ?> value="ROUTE_1">Rạch Giá - Hòn Tre và ngược lại</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_route'] == 'ROUTE_2') ? 'selected' : '' ?> value="ROUTE_2">Rạch Giá - Hòn Sơn và ngược lại</option>
                    <option <?php echo (isset($vehicledetails) && $vehicledetails[0]['v_route'] == 'ROUTE_3') ? 'selected' : '' ?> value="ROUTE_3">Tuyến khác</option>
                  </select>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <label class="form-label">Ngày đăng kiểm</label>
                <div class="form-group">
                <input type="text" name="v_date_of_registration" id="v_date_of_registration" class="form-control datepicker" placeholder="Ngày đăng kiểm" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_date_of_registration'] : '' ?>">
                  </select>
                </div>
              </div>
            </div>
            <div class="row"><div class="col-sm-6 col-md-4">
                <label class="form-label">Hạn bảo hiểm</label>
                <div class="form-group">
                <input type="text" name="v_insurance_term" id="v_insurance_term" class="form-control datepicker" placeholder="Hạn bảo hiểm" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_insurance_term'] : '' ?>">
                  </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label class="form-label">Đường dẫn API</label>
                  <input type="text" name="v_api_url" class="form-control" placeholder="API URL" value="<?php echo base_url(); ?>api/<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_api_username'] : '' ?>" readonly>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label class="form-label">Định danh</label>
                  <input type="text" id="v_api_username" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_api_username'] : '' ?>" name="v_api_username" class="form-control" placeholder="Username API" readonly>
                </div>
              </div>
              <div class="col-sm-6 col-md-4">
                <div class="form-group">
                  <label class="form-label">Mật khẩu API</label>
                  <input type="text" name="v_api_password" class="form-control" placeholder="API Password" value="<?php echo (isset($vehicledetails)) ? $vehicledetails[0]['v_api_password'] : random_string('nozero', 6) ?>" readonly>
                </div>
              </div>
            </div>
          </div>
          <hr>

      </div>
      </div>
      <input type="hidden" id="v_created_by" name="v_created_by" value="<?php echo output($this->session->userdata['session_data']['u_id']); ?>">
      <input type="hidden" id="v_created_date" name="v_created_date" value="<?php echo date('Y-m-d h:i:s'); ?>">
      <div class="card-footer text-right">
        <button type="submit" class="btn btn-primary"> <?php echo (isset($vehicledetails)) ? 'Cập nhật' : 'Thêm' ?></button>
      </div>
      </form>
      </div>
    </section>
    <!-- /.content -->