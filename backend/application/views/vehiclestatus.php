<?php
if ($this->uri->segment(2)) {
   $v_id = $this->uri->segment(2);
} else {
   $v_id = 0;
}
?>
<div class="content-header">
   <div class="container-fluid">
      <form id="track_one_vehicle" method="post">
         <div class="row col-md-12">
            <div class="col-md-5">
               <div class="form-group">
                  <select id="t_vechicle" required="true" class="form-control" name="t_vechicle">
                     <option value="">Chọn phương tiện</option>
                     <?php foreach ($vechiclelist as $key => $vechiclelists) { ?>
                        <option value="<?php echo output($vechiclelists['v_id']) ?>" <?php echo  ($vechiclelists['v_id'] == $v_id ? 'selected': '') ?>>
                           <?php echo output($vechiclelists['v_name']) . ' - ' . output($vechiclelists['v_registration_no']); ?>
                        </option>
                     <?php  } ?>
                  </select>
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group">
                  <button type="submit" class="btn btn-primary">Kiểm tra</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>



<script id="group" data-name="<?= $v_id  ?>" src="<?php echo base_url(); ?>assets/livetrack.js"></script>
<script src="<?php echo base_url(); ?>assets/fontawesome-markers.min.js"></script>
<script>

</script>
<div class="col-lg-12 col-md-12" id="map_canvas" style="width: 100%; height: 650px"></div>
</div>
</div>