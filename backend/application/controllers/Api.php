<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/RestController.php';
require APPPATH . 'libraries/Format.php';

use chriskacerguis\RestServer\RestController;

class Api extends RestController
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('api_model');
        $this->load->model('geofence_model');
    }
    public function index_get()
    {
        //$this->checkgeofence('8','22.275334996986643','70.88614147123701');
        $this->_parse_query();
        $request = $this->input->get();
        $id = isset($request['id']) ?
            $request['id'] : (isset($request['uniqueId']) ? $request['uniqueId'] : '');

        $checklogin = $this->api_model->checkgps_auth($id);
        if ($checklogin) {
            $v_id = $checklogin[0]['v_id'];
            $this->db->select("*");
            $this->db->from('positions');
            $this->db->where('v_id', $v_id);
            $this->db->limit('1');
            $this->db->where("(time < now())");
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            $lat = isset($request["lat"]) ?
                $request["lat"] : (isset($request['latitude']) ? $request['latitude'] : NULL);

            $lon = isset($request["lon"]) ?
                $request["lon"] : (isset($request['longitude']) ? $request['longitude'] : NULL);

            $previousRecords = $query->result_array();

            if ($previousRecords != null && count($previousRecords) > 0) {
                $previousRecord = $previousRecords[0];
            } else {
                $speed = 0;
            }

            $now = new DateTime();

            if ($lat != null && $lon != null && isset($previousRecord)) {

                $distance = $this->distance(
                    $previousRecord['latitude'],
                    $previousRecord['longitude'],
                    $lat,
                    $lon,
                    'K'
                );
                $previousTime = new DateTime($previousRecord['time']);
                $interval = $now->diff($previousTime);
                $deltaTimeInSeconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
                $deltaTimeInHours = $deltaTimeInSeconds / 3600;
                $speed = $distance / ($deltaTimeInHours * 1.852);  // by knot
            }

            $altitude = isset($request["altitude"]) ? $request["altitude"] : NULL;
            $bearing = isset($request["bearing"]) ? $request["bearing"] : NULL;
            $accuracy = isset($request["accuracy"]) ? $request["accuracy"] : NULL;
            $comment = isset($request["comment"]) ? $request["comment"] : NULL;
            $postarray = array('v_id' => $v_id, 'latitude' => $lat, 'longitude' => $lon, 'time' => $now->format('Y-m-d h:i:s'), 'altitude' => $altitude, 'speed' => $speed, 'bearing' => $bearing, 'accuracy' => $accuracy, 'comment' => $comment);
            $this->api_model->add_postion($postarray);
            $this->checkgeofence($v_id, $lat, $lon);
            $response = array('error' => false, 'message' => ['v_id' => $v_id]);
            $this->response($response, 200);
        }
    }

    public function index_post()   //Get GPS feed in device
    {
        $this->_parse_query();
        $request = $this->input->get();
        $id = isset($request['id']) ?
            $request['id'] : (isset($request['uniqueId']) ? $request['uniqueId'] : '');

        $checklogin = $this->api_model->checkgps_auth($id);

        if ($checklogin) {
            $v_id = $checklogin[0]['v_id'];
            $this->db->select("*");
            $this->db->from('positions');
            $this->db->where('v_id', $v_id);
            $this->db->limit('1');
            $this->db->where("(time < now())");
            $this->db->order_by('id', 'desc');
            $query = $this->db->get();
            $lat = isset($request["lat"]) ?
                $request["lat"] : (isset($request['latitude']) ? $request['latitude'] : NULL);

            $lon = isset($request["lon"]) ?
                $request["lon"] : (isset($request['longitude']) ? $request['longitude'] : NULL);

            $previousRecords = $query->result_array();

            if ($previousRecords != null && count($previousRecords) > 0) {
                $previousRecord = $previousRecords[0];
            } else {
                $speed = 0;
            }

            $now = new DateTime();

            if ($lat != null && $lon != null && isset($previousRecord)) {

                $distance = $this->distance(
                    $previousRecord['latitude'],
                    $previousRecord['longitude'],
                    $lat,
                    $lon,
                    'K'
                );
                $previousTime = new DateTime($previousRecord['time']);
                $interval = $now->diff($previousTime);
                $deltaTimeInSeconds = $interval->days * 86400 + $interval->h * 3600 + $interval->i * 60 + $interval->s;
                $deltaTimeInHours = $deltaTimeInSeconds / 3600;
                $speed = $distance / ($deltaTimeInHours * 1.852);  // by knot
            }

            $altitude = isset($request["altitude"]) ? $request["altitude"] : NULL;
            $bearing = isset($request["bearing"]) ? $request["bearing"] : NULL;
            $accuracy = isset($request["accuracy"]) ? $request["accuracy"] : NULL;
            $comment = isset($request["comment"]) ? $request["comment"] : NULL;
            $postarray = array('v_id' => $v_id, 'latitude' => $lat, 'longitude' => $lon, 'time' => $now->format('Y-m-d h:i:s'), 'altitude' => $altitude, 'speed' => $speed, 'bearing' => $bearing, 'accuracy' => $accuracy, 'comment' => $comment);
            $this->api_model->add_postion($postarray);
            $this->checkgeofence($v_id, $lat, $lon);
            $response = array('error' => false, 'message' => ['v_id' => $v_id]);
            $this->response($response, 200);
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function positions_post()     //Postion feed to front end   
    {
        $this->db->select("*");
        $this->db->from('positions');
        $this->db->where('v_id', $this->post('t_vechicle'));
        $this->db->where('date(time) >=', $this->post('fromdate'));
        $this->db->where('date(time) <=', $this->post('todate'));
        $query = $this->db->get();
        $data = $query->result_array();
        $distancefrom = reset($data);
        $distanceto = end($data);
        $totaldist = $this->totaldistance($distancefrom, $distanceto);
        $returndata = array('status' => 1, 'data' => $data, 'totaldist' => $totaldist, 'message' => 'data');
        $this->response($returndata, 200);
    }

    public function totaldistance($distancefrom, $distanceto, $earthRadius = 6371000)
    {
        $latFrom = deg2rad($distancefrom['latitude']);
        $lonFrom = deg2rad($distancefrom['longitude']);
        $latTo = deg2rad($distanceto['latitude']);
        $lonTo = deg2rad($distanceto['longitude']);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function currentpositions_get()
    {
        $data = array();
        $postions = array();
        $this->db->select("p.*,v.v_name,v.v_type,v.v_color");
        $this->db->from('positions p');
        $this->db->join('vehicles v', 'v.v_id = p.v_id');
        $this->db->where('v.v_is_active', 1);

        if (isset($_GET['uname'])) {
            $this->db->where('v.v_api_username', $_GET['uname']);
        }

        if (isset($_GET['gr'])) {
            $this->db->where('v.v_group', $_GET['gr']);
        }

        if (isset($_GET['v_id'])) {
            $this->db->where('v.v_id', $_GET['v_id']);
        }

        $this->db->where('`id` IN (SELECT MAX(id) FROM positions GROUP BY `v_id`)', NULL, FALSE);
        $query = $this->db->get();
        $data = $query->result_array();
        if (count($data) >= 1) {
            $resp = array('status' => 1, 'data' => $data);
        } else {
            $resp = array('status' => 0, 'message' => 'Không có tín hiệu GPS nào');
        }
        $this->response($resp, 200);
    }
    public function checkgeofence($vid, $lat, $log)
    {
        $vgeofence = $this->geofence_model->getvechicle_geofence($vid);
        if (!empty($vgeofence)) {
            $points = array("$lat $log");
            foreach ($vgeofence as $geofencedata) {
                $lastgeo = explode(" ,", $geofencedata['geo_area']);
                $polygon = $geofencedata['geo_area'] . $lastgeo[0];
                $polygondata = explode(' , ', $polygon);
                foreach ($polygondata as $polygoncln) {
                    $geopolygondata[] = str_replace(",", ' ', $polygoncln);
                }
                foreach ($points as $key => $point) {
                    $geocheck = pointInPolygon($point, $geopolygondata, false);
                    if ($geocheck == 'outside' || $geocheck == 'boundary' || $geocheck == 'inside') {
                        $wharray = array(
                            'ge_v_id' => $vid, 'ge_geo_id' => $geofencedata['geo_id'], 'ge_event' => $geocheck,
                            'DATE(ge_timestamp)' => date('Y-m-d')
                        );
                        $geofence_events = $this->db->select('*')->from('geofence_events')->where($wharray)->get()->result_array();

                        if (count($geofence_events) == 0) {
                            $insertarray = array('ge_v_id' => $vid, 'ge_geo_id' => $geofencedata['geo_id'], 'ge_event' => $geocheck, 'ge_timestamp' =>
                            date('Y-m-d h:i:s'));
                            $this->db->insert('geofence_events', $insertarray);
                        }
                    }
                }
            }
        }
    }
}
