<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	 function __construct()
     {
          parent::__construct();
          $this->load->database();
          $this->load->model('user_model');
          $this->load->helper(array('form', 'url','string'));
          $this->load->library('form_validation');
          $this->load->library('session');
     }

	public function index()
	{
		$data['userlist'] = $this->user_model->getall_user();
		$this->template->template_render('user_management',$data);
	}
	public function adduser()
	{
		$this->template->template_render('user_add');
	}
	public function insertuser() 
	{
		if(isset($_POST)){
			$user = $this->input->post();

			// Set default permissions
			$user['permissions']['lr_drivers_list'] = 1;
			$user['permissions']['lr_drivers_list_edit'] = 1;
			$user['permissions']['lr_drivers_add'] = 1;
			$user['permissions']['lr_trips_list'] = 1;
			$user['permissions']['lr_trips_list_edit'] = 1;
			$user['permissions']['lr_trips_add'] = 1;
			$user['permissions']['lr_cust_list'] = 1;
			$user['permissions']['lr_cust_edit'] = 1;
			$user['permissions']['lr_cust_add'] = 1;
			$user['permissions']['lr_fuel_list'] = 1;
			$user['permissions']['lr_fuel_edit'] = 1;
			$user['permissions']['lr_fuel_add'] = 1;
			$user['permissions']['lr_reminder_list'] = 1;
			$user['permissions']['lr_reminder_delete'] = 1;
			$user['permissions']['lr_reminder_add'] = 1;
			$user['permissions']['lr_ie_list'] = 1;
			$user['permissions']['lr_ie_edit'] = 1;
			$user['permissions']['lr_ie_add'] = 1;

			$response = $this->user_model->add_user($user);
			if($response) {
				$this->session->set_flashdata('successmessage', 'Thêm người dùng thành công..');
			} else {
				$this->session->set_flashdata('warningmessage', 'Có lỗi xảy ra khi thêm người dùng..');
			}
			redirect('users');
		} else {
			$this->session->set_flashdata('warningmessage', 'Lỗi nhập liệu! Xin hãy thử lại');
			redirect('users');
		}
	}
	public function edituser()
	{
		$u_id = $this->uri->segment(3);
		$data['userdetails'] = $this->user_model->get_userdetails($u_id);
		$this->template->template_render('user_add',$data);
	}

	public function updateuser()
	{
		if(isset($_POST)){
			$response = $this->user_model->update_user($this->input->post());
				if($response) {
					$this->session->set_flashdata('successmessage', 'Cập nhật người dùng thành công..');
				    redirect('users');
				} else
				{
					$this->session->set_flashdata('warningmessage', 'Có lỗi xảy ra! Xin hãy thử lại');
				    redirect('users');
				}
		} else {
			$this->session->set_flashdata('warningmessage', 'Lỗi nhập liệu! Xin hãy thử lại');
			redirect('users');
		}
	}
}
