<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VehicleStatus extends CI_Controller {

	 function __construct()
     {
          parent::__construct();
          $this->load->database();
          $this->load->helper('url');
          $this->load->library('session');
     }

	public function index($id = 0)
	{
        $this->load->model('trips_model');
		$data['vechiclelist'] = $this->trips_model->getall_vechicle();

        $key = $this->db->select('s_googel_api_key')->from('settings')->get()->result_array();
        if(isset($key[0]['s_googel_api_key']) && $key[0]['s_googel_api_key']!='') {
            $this->template->template_render('vehiclestatus', $data);
        } else {
            $this->session->set_flashdata('warningmessage', 'Please add google map key in settings page');
		    $this->template->template_render('vehiclestatus', $data);
        }
	}
}
