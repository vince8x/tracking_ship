<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Vehicle extends CI_Controller {
	function __construct()
    {
          parent::__construct();
          $this->load->database();
          $this->load->model('vehicle_model');
          $this->load->model('incomexpense_model');
          $this->load->model('geofence_model');
          $this->load->helper(array('form', 'url','string'));
          $this->load->library('form_validation');
          $this->load->library('session');
    }
	public function index()
	{
		$data['vehiclelist'] = $this->vehicle_model->getall_vehicle();
		$this->template->template_render('vehicle_management',$data);
	}
	public function addvehicle()
	{
		$data['v_group'] = $this->vehicle_model->get_vehiclegroup();
		$this->template->template_render('vehicle_add',$data);
		
		
	}
	public function insertvehicle()
	{
		$this->form_validation->set_rules('v_registration_no','Registration Number','required|trim|is_unique[vehicles.v_registration_no]');
		$this->form_validation->set_message('is_unique', '%s is already exist');
		$this->form_validation->set_rules('v_model','Model','');
		$this->form_validation->set_rules('v_chassis_no','Chassis No','');
        $this->form_validation->set_rules('v_engine_no', 'Engine No', '');
		$this->form_validation->set_rules('v_manufactured_by','Manufactured By','required|trim');
		$this->form_validation->set_rules('v_type','Vehicle Type','required|trim');
		$this->form_validation->set_rules('v_color','Vehicle Color','required|trim');
		$testxss = xssclean($_POST);
		if($this->form_validation->run()==TRUE && $testxss){
			$input_data = $this->input->post();
			$input_data['v_model'] = '';
			$input_data['v_chassis_no'] = '';
			$input_data['v_engine_no'] = '';
			$response = $this->vehicle_model->add_vehicle($input_data);
			if($response) {
				$this->session->set_flashdata('successmessage', 'Thêm phương tiện thành công..');
			    redirect('vehicle');
			} 
		} 
		else	
		{
			$errormsg = validation_errors();
			if(!$testxss) {
				$errormsg = 'Lỗi nhập liệu! Xin hãy thử lại';
			} 
			$this->session->set_flashdata('warningmessage',$errormsg);
			redirect('vehicle/addvehicle');
		} 
	}
	public function editvehicle()
	{
		$v_id = $this->uri->segment(3);
		$data['v_group'] = $this->vehicle_model->get_vehiclegroup();
		$data['vehicledetails'] = $this->vehicle_model->get_vehicledetails($v_id); 
		$this->template->template_render('vehicle_add',$data);
	}

	public function updatevehicle()
	{
		$testxss = xssclean($_POST);
		if($testxss){
			$response = $this->vehicle_model->edit_vehicle($this->input->post());
				if($response) {
					$this->session->set_flashdata('successmessage', 'Cập nhật phương tiện thành công..');
				    redirect('vehicle');
				} else
				{
					$this->session->set_flashdata('warningmessage', 'Có lỗi xảy ra! xin hãy thử lại');
				    redirect('vehicle');
				}
		} else {
			$this->session->set_flashdata('warningmessage', 'Lỗi nhập liệu! Xin hãy thử lại');
			redirect('vehicle');
		}
	}
	public function deletevehicle()
	{
		$v_id = $this->uri->segment(3);
		$returndata = $this->vehicle_model->delete_vehicle($v_id);
		if($returndata) {
			$this->session->set_flashdata('successmessage', 'Xóa phương tiện thành công..');
			redirect('vehicle');
		} else {
			$this->session->set_flashdata('warningmessage', 'Lỗi..! Có lỗi xảy ra khi xóa phương tiện');
		    redirect('vehicle');
		}
	}
	public function viewvehicle()
	{
		$v_id = $this->uri->segment(3);
		$vehicledetails = $this->vehicle_model->get_vehicledetails($v_id);
		$bookings = $this->vehicle_model->getall_bookings($v_id);
		$vgeofence = $this->geofence_model->getvechicle_geofence($v_id);
		$vincomexpense = $this->incomexpense_model->getvechicle_incomexpense($v_id);
		$geofence_events = $this->geofence_model->countvehiclengeofence_events($v_id);
		if(isset($vehicledetails[0]['v_id'])) {
			$data['vehicledetails'] = $vehicledetails[0];
			$data['bookings'] = $bookings;
			$data['vechicle_geofence'] = $vgeofence;
			$data['vechicle_incomexpense'] = $vincomexpense;
			$data['geofence_events'] = $geofence_events;
			$this->template->template_render('vehicle_view',$data);
		} else {
			$this->template->template_render('pagenotfound');
		}
	}
	public function vehiclegroup()
	{
		$data['vehiclegroup'] = $this->vehicle_model->get_vehiclegroup();
		$this->template->template_render('vehicle_group',$data);
	}
	public function vehiclegroup_delete()
	{
		$gr_id = $this->uri->segment(3);
		$returndata = $this->vehicle_model->vehiclegroup_delete($gr_id);
		if($returndata) {
			$this->session->set_flashdata('successmessage', 'Xóa thành công..');
			redirect('vehicle/vehiclegroup');
		} else {
			$this->session->set_flashdata('warningmessage', 'Có lỗi xảy ra! Xin hãy xóa phương tiện được gắn với số ghế/tấn đăng kiểm trước');
		    redirect('vehicle/vehiclegroup');
		}
	}
	public function addgroup()
	{
		$response = $this->db->insert('vehicle_group',$this->input->post());
		if($response) {
			$this->session->set_flashdata('successmessage', 'Thêm thành công..');
		    redirect('vehicle/vehiclegroup');
		} else
		{
			$this->session->set_flashdata('warningmessage', 'Có lỗi xảy ra! Xin hãy thử lại');
		    redirect('vehicle/vehiclegroup');
		}
	}
}
