var map = infoWindow = latitude = null;
var markersArray = [];
window.onload = function () {

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      document.cookie = "latitude=" + position.coords.latitude;
      document.cookie = "longitude=" + position.coords.longitude;
    });
  }
  map = new google.maps.Map(document.getElementById("map_canvas"), {
    center: new google.maps.LatLng(52.696361078274485, -111.4453125),
    zoom: 3,
    mapTypeId: 'roadmap',
    gestureHandling: 'greedy'
  });
  infoWindow = new google.maps.InfoWindow;
  livetracking();
  window.setInterval(livetracking, 15000);
};

function livetracking() {
  if ($("#t_vechicle").val()) {
    var path = $('#base').val() + "/api/currentpositions?v_id=" + $("#t_vechicle").val();
  } else {
    var path = $('#base').val() + "/api/currentpositions";
  }

  $.ajax({
    type: "GET",
    url: path,
    dataType: 'json',
    cache: false,
    success: function (result) {
      if (result.status == 1) {
        var j;
        var markers = result.data;
        var bounds = new google.maps.LatLngBounds();
        resetMarkers(markersArray)
        for (i = 0; i < markers.length; i++) {
          var lastupdate = markers[i].time;
          var v_type = 'M404.5,269.5V103.7h-19.8v165.8h-69.8V167.3H295v102.2h-36.9l-65.2-147h-32.4L151.2,61H82.4L73,122.5H41.7v147H0L36.5,429 h334.6L490,269.5H404.5z M100.1,80.8h33.4l6.3,41.7H93.9L100.1,80.8z M62.6,143.4h116.7l20.9,46.9H62.6V143.4z M209.1,210.1 l26.5,59.4h-173v-59.4H209.1z M361.8,408.1H53.2L25,289.3h425.3L361.8,408.1z';
          var point = new google.maps.LatLng(parseFloat(markers[i].latitude), parseFloat(markers[i].longitude));
          var html = "<div class=' '><b>" + "Tên: </b>" + markers[i].v_name + "<br>" + 
          "<b>Kinh độ:</b> " + convertToDMS(markers[i].longitude) +  '<br><b>Vĩ độ: </b>' + convertToDMS(markers[i].latitude) + "<br>" +
            "<b>Vận tốc: </b>" + (Math.round(markers[i].speed * 10000 + Number.EPSILON) / 10000) +
            " MN<br>" + "<b>Cập nhật: </b>" + lastupdate + "<br></div>";
          var marker = new google.maps.Marker({
            map: map,
            position: point,
            icon: {
              path: v_type,
              scale: 0.05,
              strokeWeight: 0.2,
              strokeColor: 'black',
              strokeOpacity: 1,
              fillColor: markers[i].v_color,
              fillOpacity: 2.0,
            },
          });
          markersArray.push(marker);
          bindInfoWindow(marker, map, infoWindow, html);
        }
      } else {
        alertmessage(result.message, 2);
      }
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log('Unexpected error.');
    }
  });
}

// Convert Dd to DMS 
function convertToDMS(decimalValue) {
  var dms = Math.abs(decimalValue);
  //degree
  var deg = dms | 0;
  var frac = dms - deg;
  // minute
  var min = (frac * 60) | 0;
  //second
  var sec = frac * 3600 - min * 60;
  sec = Math.round(sec * 100) / 100;
  return deg + "°" + min + "'" + sec + '"';
}

function resetMarkers(arr) {
  for (var i = 0; i < arr.length; i++) {
    arr[i].setMap(null);
  }
  arr = [];
}
function bindInfoWindow(marker, map, infoWindow, html) {
  google.maps.event.addListener(marker, 'click', function () {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });

}
function alertmessage(msg, type) {
  if (type == 1) {
    $.toast({
      heading: 'Success',
      text: msg,
      icon: 'info',
      loader: true,
      position: 'top-center',
      loaderBg: '#2196f3',
      afterHidden: function () {
        location.reload();
      },
    });

  }
  if (type == 2) {
    $.toast({
      heading: 'Error',
      text: msg,
      icon: 'error',
      loader: true,
      position: 'top-center',
      loaderBg: '#f44336',
    });
  }

}
